﻿The MD4.alg program describes the algorithm computing the MD4 hash
function, that transforms 1 block (512 bit) to 128-bit hash value. The
result of the translation of this program is the CNF MD4_1.cnf. In
this CNF the original message block (512 bit) is encoded by the first
512 Boolean variables (number 1 .. 512). The corresponding hash value
(128 bit) is encoded by the last 128 Boolean variables (number 8630 ..
8757).

To construct the CNF for finding the preimage one should add to the
CNF the information about the known hash value (via unit clauses).
If the satisfying assignment is found then the preimage corresponds to
the assignment of variables number 1 .. 512.

The MD4_collision.alg program describes the problem of search for
single-block collisions for the MD4 hash function. Essentially it
implements the method proposed by X. Wang for the considered problem.
The result of the translation of this program is the CNF in which all
necessary conditions (i.e. constraints on the differential path, etc.)
are already present. Therefore, one can immediately start solving it
using a SAT solver. In the satisfying assignment the first message
corresponds to the assignment of first 512 Boolean variables. To
produce the second message (the collision) one needs to subtract the
known constants (found in the work by X. Wang).